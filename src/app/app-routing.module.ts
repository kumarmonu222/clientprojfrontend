import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagenotfoundComponent } from './_components/pagenotfound/pagenotfound.component';
import { SellerGuard } from './_guards/seller.guard';
import { ProfileComponent } from './_components/profile/profile.component';
import { LoginComponent } from './_components/login/login.component';
import { NavbarComponent } from './_components/navbar/navbar.component';
import { DashboardComponent } from './_components/dashboard/dashboard.component';
import { ShopComponent } from './_components/shop/shop.component';
import { BijakComponent } from './_components/bijak/bijak.component';
import { AllBijakComponent } from './_components/all-bijak/all-bijak.component';
import { AddBijakComponent } from './_components/add-bijak/add-bijak.component';
import { EditBijakComponent } from './_components/edit-bijak/edit-bijak.component';
import { SellReportComponent } from './_components/sell-report/sell-report.component';
import { AllSellReportComponent } from './_components/all-sell-report/all-sell-report.component';
import { EditSellReportComponent } from './_components/edit-sell-report/edit-sell-report.component';
import { AddSellReportComponent } from './_components/add-sell-report/add-sell-report.component';
import { HomepageComponent } from './_components/homepage/homepage.component';

const routes: Routes = [
  {path: '', component:HomepageComponent},
  {path: 'login', component:LoginComponent},
  {path: 'dashboard', component:NavbarComponent,
    canActivate: [SellerGuard],
    children: [
      {path:'', component: DashboardComponent},
      {path:'1', component: DashboardComponent},
      {path:'profile', component: ProfileComponent},
      {path:'shop', component: ShopComponent},
      {path:'bijak', component: BijakComponent,
        children: [
          { path: '', redirectTo: 'all-bijak', pathMatch: 'full' },
          { path: 'all-bijak', component: AllBijakComponent },
          { path:'edit-bijak', component: EditBijakComponent },
          { path:'add-bijak', component: AddBijakComponent },
        ]
      },
      {path:'sell-report', component: SellReportComponent,
        children: [
          { path: '', redirectTo: 'all-sell-report', pathMatch: 'full' },
          { path: 'all-sell-report', component: AllSellReportComponent },
          { path:'edit-sell-report', component: EditSellReportComponent },
          { path:'add-sell-report', component: AddSellReportComponent },
        ]
      },
    ]
  },
  {path: '**', component: PagenotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
