import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from "ngx-spinner";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PagenotfoundComponent } from './_components/pagenotfound/pagenotfound.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AngularMaterialModule } from './angular-material/angular-material.module';
import { httpInterceptorProviders } from './_helpers';
import { NavbarComponent } from './_components/navbar/navbar.component';
import { ProfileComponent } from './_components/profile/profile.component';
import { environment } from 'src/environments/environment';
import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider } from "angularx-social-login";
import { NgxPrintModule } from 'ngx-print';
import { LoginComponent } from './_components/login/login.component';
import { DashboardComponent } from './_components/dashboard/dashboard.component';
import { HomepageComponent } from './_components/homepage/homepage.component';
import { CKEditorModule } from 'ng2-ckeditor';
import { ShopComponent } from './_components/shop/shop.component';
import { BijakComponent } from './_components/bijak/bijak.component';
import { AddBijakComponent } from './_components/add-bijak/add-bijak.component';
import { EditBijakComponent } from './_components/edit-bijak/edit-bijak.component';
import { AllBijakComponent } from './_components/all-bijak/all-bijak.component';
import { SellReportComponent } from './_components/sell-report/sell-report.component';
import { AddSellReportComponent } from './_components/add-sell-report/add-sell-report.component';
import { EditSellReportComponent } from './_components/edit-sell-report/edit-sell-report.component';
import { AllSellReportComponent } from './_components/all-sell-report/all-sell-report.component';
import { NavmenuComponent } from './_components/navmenu/navmenu.component';
import { FooterComponent } from './_components/footer/footer.component';

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider(environment.googleClientId)
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider(environment.facebookClientId)
  },
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    PagenotfoundComponent,
    NavbarComponent,
    HomepageComponent,
    ProfileComponent,
    LoginComponent,
    DashboardComponent,
    ShopComponent,
    BijakComponent,
    AddBijakComponent,
    EditBijakComponent,
    AllBijakComponent,
    SellReportComponent,
    AddSellReportComponent,
    EditSellReportComponent,
    AllSellReportComponent,
    NavmenuComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    SocialLoginModule,
    CKEditorModule,
    NgxSpinnerModule,
    NgxPrintModule,
    ToastrModule.forRoot(),
    AngularMaterialModule
  ],
  providers: [
    httpInterceptorProviders,
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }