import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { CommonServicesService } from 'src/app/_services/commonservice.service';
import { PusherService } from 'src/app/_services/pusher.service';
import * as jwt_decode from 'jwt-decode';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {
  shopDetails: any;
  images: any;
  bijakImage: string;

  constructor(private formbuilder: FormBuilder,
    private _http:HttpClient,
    private router: Router, 
    private pusherService: PusherService,
    public toastr: ToastrService,
    private commonServ: CommonServicesService) { }

  ngOnInit() {
    this.commonServ.getSingle('api/shop/detail', jwt_decode(localStorage.getItem('token')).id).subscribe((data:any) => {
      this.shopDetails = data.shop;
    }); 
    this.bijakImage = localStorage.getItem('bijakImage');
  }

  selectImage(event) {
    if(event.target.files.length > 0) {
      const file = event.target.files[0];
      this.images = file;
    }
  }

  uploadBijakImage() {
    const formData = new FormData();
    formData.append('bijakImage', this.images);
    this._http.post<any>(`${environment.baseUrl}api/user/upload-bijak-image`, formData).subscribe((data) => {
      localStorage.setItem('bijakImage', data.user.bijakImage);
      this.ngOnInit();
    }, (error) => {
      console.log(error);
    });
  }
}
