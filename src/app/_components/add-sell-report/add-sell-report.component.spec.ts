import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSellReportComponent } from './add-sell-report.component';

describe('AddSellReportComponent', () => {
  let component: AddSellReportComponent;
  let fixture: ComponentFixture<AddSellReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSellReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSellReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
