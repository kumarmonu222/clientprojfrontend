import { Component, OnInit } from '@angular/core';
import { CommonServicesService } from 'src/app/_services/commonservice.service';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import * as jwt_decode from 'jwt-decode';
import * as $ from "jquery";
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-sell-report',
  templateUrl: './add-sell-report.component.html',
  styleUrls: ['./add-sell-report.component.scss']
})
export class AddSellReportComponent implements OnInit {
  addSellReportForm: FormGroup;
  itemRecieved: FormArray;
  itemSold: FormArray;
  submitted: boolean;
  shopDetails: any;

  constructor(
    private commonServ: CommonServicesService,
    private formbuilder: FormBuilder, 
    public toastr: ToastrService,
    private router:Router
  ) { 
    this.addSellReportForm = this.formbuilder.group({ 
      itemRecieved: this.formbuilder.array([ this.createRecievedItem() ]),
      itemSold: this.formbuilder.array([ this.createSoldItem() ])
    });
  }

  createRecievedItem(): FormGroup {
    return this.formbuilder.group({
        sno: '',
        merchant: '',
        city: '',
        truckNo: '',
        description: '',
        nagsRecieved: '',
        nagsRecievedOutOf: '',
    });
  }

  createSoldItem(): FormGroup {
    return this.formbuilder.group({
        sno: '',
        retailer: '',
        address: '',
        description: '',
        nagsSold: '',
    });
  }

  addRecievedItem(): void {
    this.itemRecieved = this.addSellReportForm.get('itemRecieved') as FormArray;
    this.itemRecieved.push(this.createRecievedItem());
  }

  addSoldItem(): void {
    this.itemSold = this.addSellReportForm.get('itemSold') as FormArray;
    this.itemSold.push(this.createSoldItem());
  }

  ngOnInit() {
    this.commonServ.getSingle('api/shop/detail', jwt_decode(localStorage.getItem('token')).id).subscribe((data:any) => {
      this.shopDetails = data.shop;
    }); 
  }

  
  onReset() {
    this.submitted = false;
    this.addSellReportForm.reset();
  }

  onAddSellReportSubmit() {
    this.submitted = true;
    this.addSellReportForm.value.shopId = this.shopDetails._id;
    console.log(this.addSellReportForm.value);
    this.commonServ.post('api/sell-report', this.addSellReportForm.value).subscribe((data: any) => {
      this.toastr.success('SellReport creted successfully');
      // this.router.navigate(['dashboard/SellReport/all-sell-report']);

    }, (error) => {
      console.log(error);
    });
  }

}