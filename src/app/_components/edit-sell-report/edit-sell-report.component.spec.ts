import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSellReportComponent } from './edit-sell-report.component';

describe('EditSellReportComponent', () => {
  let component: EditSellReportComponent;
  let fixture: ComponentFixture<EditSellReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSellReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSellReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
