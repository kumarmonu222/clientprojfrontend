import { Component, OnInit } from '@angular/core';
import { CommonServicesService } from 'src/app/_services/commonservice.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import * as jwt_decode from 'jwt-decode';
import { NgxSpinnerService } from "ngx-spinner";


@Component({
  selector: 'app-all-sell-report',
  templateUrl: './all-sell-report.component.html',
  styleUrls: ['./all-sell-report.component.scss']
})
export class AllSellReportComponent implements OnInit {
  products: any;
  shopId: any;
  sellReports: any;
  singlesellReport: any;
  selected: any;

  constructor(
    private commonServ: CommonServicesService,
    private formbuilder: FormBuilder, 
    public toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.commonServ.getSingle('api/shop/detail', jwt_decode(localStorage.getItem('token')).id).subscribe((data:any) => {
      this.shopId = data.shop._id;
      this.commonServ.getAll('api/sell-report/specific-shop-sellReports/' + this.shopId).subscribe((data: any) =>{
        this.sellReports = data.sellReport;
        this.spinner.hide();
      }, (error:any) => {
        console.log();
        if(error.error.message === "No Entries found") {
          this.spinner.hide();
        }
      });
    }); 
  }

  getSellReport(sellReportId) {
    this.commonServ.getSingle('api/sell-report/', sellReportId).subscribe((data: any) => {
      this.singlesellReport = data.sellReport;
      this.selected = data.sellReport._id;
      console.log(this.singlesellReport);
    });
  }

  isActive(item) {
    return this.selected === item;
  };
  
  deletesellReport(id) {
    this.commonServ.delete('api/sellReport/delete', id).subscribe(
      (data:any) => {
        this.toastr.success(data.success);
        this.ngOnInit();
      },
      error => {
        this.toastr.error(error.error.message);
      }
    );
  }

}
