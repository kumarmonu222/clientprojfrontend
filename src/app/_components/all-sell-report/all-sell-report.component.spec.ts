import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllSellReportComponent } from './all-sell-report.component';

describe('AllSellReportComponent', () => {
  let component: AllSellReportComponent;
  let fixture: ComponentFixture<AllSellReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllSellReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllSellReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
