import { Component, OnInit } from '@angular/core';
import { CommonServicesService } from 'src/app/_services/commonservice.service';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import * as jwt_decode from 'jwt-decode';
import * as $ from "jquery";
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-bijak',
  templateUrl: './add-bijak.component.html',
  styleUrls: ['./add-bijak.component.scss']
})
export class AddBijakComponent implements OnInit {
  addBijakForm: FormGroup;
  itemRows: FormArray;
  submitted: boolean;
  shopDetails: any;

  constructor(
    private commonServ: CommonServicesService,
    private formbuilder: FormBuilder, 
    public toastr: ToastrService,
    private router:Router
  ) { 
    this.addBijakForm = this.formbuilder.group({ 
      no: ['', [Validators.required]],
      city: ['', [Validators.required]],
      ms: ['', [Validators.required]],
      dateOfReciept: ['', [Validators.required]],
      dispatchDate: ['', [Validators.required]],
      truckNo: ['', [Validators.required]],
      truckFreight: [''],
      railFreight: [''],
      stationMandiExp: [''],
      unloading: [''],
      postage: [''],
      phone: [''],
      coldStorage: [''],
      loading: [''],
      charity: [''],
      itemRows: this.formbuilder.array([ this.createItem() ])
    });
  }

  createItem(): FormGroup {
    return this.formbuilder.group({
      sno: '',
      description: '',
      ctnKg: '',
      rate: '',
      itemAmount: ''
    });
  }

  addItem(): void {
    this.itemRows = this.addBijakForm.get('itemRows') as FormArray;
    this.itemRows.push(this.createItem());
  }

  ngOnInit() {
    this.commonServ.getSingle('api/shop/detail', jwt_decode(localStorage.getItem('token')).id).subscribe((data:any) => {
      this.shopDetails = data.shop;
    }); 
  }

  
  onReset() {
    this.submitted = false;
    this.addBijakForm.reset();
  }

  onAddBijakSubmit() {
    this.submitted = true;
    this.addBijakForm.value.shopId = this.shopDetails._id;
    console.log(this.addBijakForm.value);
    this.commonServ.post('api/bijak', this.addBijakForm.value).subscribe((data: any) => {
      this.toastr.success('Bijak creted successfully');
      this.router.navigate(['dashboard/bijak/all-bijak']);

    }, (error) => {
      console.log(error);
    });
  }

}