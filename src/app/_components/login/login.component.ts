import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { NavbarService } from 'src/app/_services/navbar.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import * as jwt_decode from 'jwt-decode';
import { ToastrService } from 'ngx-toastr';
import { AuthService, GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';
import { CommonServicesService } from 'src/app/_services/commonservice.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm:FormGroup;
  forgotPasswordForm:FormGroup;
  signupForm:FormGroup;
  is_logged_in: boolean;
  signin:boolean = true;
  forgotPassword:boolean = false;
  signup:boolean = false;
  userData: {};
  
  constructor(
    private formbuilder: FormBuilder, 
    private router: Router, 
    public toastr: ToastrService,
    private authService: AuthService,
    private navbarServ: NavbarService,
    private commonServ: CommonServicesService
    ) { }

  ngOnInit() {
    this.navbarServ.hide();
    // login form
    $(function(){
      $(".borderbtm input").on("focus", function(){
          $(this).addClass("focus");
      });
      $(".borderbtm input").on("blur", function(){
          if($(this).val()  == "")
              $(this).removeClass("focus");
      });
    });
    this.navbarServ.logout();

    this.loginForm = this.formbuilder.group({ 
      email: ['', [Validators.required, Validators.email, Validators.email, Validators.pattern('^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      password: ['', [Validators.required, Validators.minLength(9)]]
    });

    this.forgotPasswordForm = this.formbuilder.group({ 
      email: ['', [Validators.required, Validators.email, Validators.email, Validators.pattern('^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
    });

    this.signupForm = this.formbuilder.group({ 
      firstName:['', [Validators.required]],
      username:['', [Validators.required]],
      email: ['', [Validators.required, Validators.email, Validators.email, Validators.pattern('^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      password: ['', [Validators.required, Validators.minLength(9)]]
      // phone: ['', [Validators.required, Validators.min(1000000000), Validators.max(999999999999)]],
    });
  }

  onSignupSubmit() {
    let userData = this.signupForm.value;
    this.commonServ.post('api/auth/register', userData).subscribe(
      (data:any) => {
        this.toastr.success(data.message);
      },
      error => {
        this.toastr.error(error.error.message);
      }
    );
  }

  onForgotPasswordSubmit() {
    let userData = this.forgotPasswordForm.value;
    this.commonServ.post('api/auth/recover', userData).subscribe(
      (data:any) => {
        this.toastr.success(data.message);
      },
      error => {
        this.toastr.error(error.error.message);
      }
    );
  }

  onLoginSubmit(){
    let userData = this.loginForm.value;
    this.commonServ.post('api/auth/login', userData).subscribe(
      (data:any) => {
        localStorage.setItem('token', JSON.stringify(data.token));
        localStorage.setItem('currentUser', jwt_decode(data.token).email);
        // this.navbarServ.isLoggedIn();
        console.log(jwt_decode(data.token));
        if(jwt_decode(data.token).is_admin) {
          this.toastr.success('Hello Admin! Login to Admin dashboard');
          this.logout();
        } else if(jwt_decode(data.token).is_seller) {
          this.toastr.success('login successfull');
          this.router.navigate(['/dashboard']);
        } else {
          this.toastr.success('Hello User! Login to User dashboard');
          this.logout();
        }
      },
      error => {
        this.toastr.error(error.error.message);
      }
    );
  }

  logout() {
    this.navbarServ.logout();
  }

  socialLogin(provider) {
    if(provider == 'google') {
      this.authService
      .signIn(GoogleLoginProvider.PROVIDER_ID)
      .then((data:any) => {
        this.userData = {
          first_name: data.firstName,
          last_name: data.lastName,
          username: data.name,
          email: data.email,
          profileImage: data.photoUrl,
          user_type: 'user',
          isVerified: true,
          token: data.idToken,
          provider: data.provider
        }
        this.commonServ.post('api/auth/social', this.userData).subscribe((data:any) => {
          localStorage.setItem('token', JSON.stringify(data.token));
          localStorage.setItem('currentUser', jwt_decode(data.token).email);
          localStorage.setItem('bijakImage', data.user.bijakImage);
          if(jwt_decode(data.token).is_admin) {
            this.toastr.success('Hello Admin! Login to Admin dashboard');
            this.logout();
          } else if(jwt_decode(data.token).is_seller) {
            this.toastr.success('login successfull');
            this.router.navigate(['/dashboard']);
          } else {
            this.toastr.success('Hello User! Login to User dashboard');
            this.logout();
          }
        },
        error => {
          this.toastr.error(error.error.message);
        });
      });
    } else if(provider == 'facebook') {
      this.authService
      .signIn(FacebookLoginProvider.PROVIDER_ID)
      .then((data:any) => {
        console.log(data);
        this.userData = {
          first_name: data.firstName,
          last_name: data.lastName,
          email: data.email,
          profileImage: data.photoUrl,
          user_type: 'user',
          isVerified: true,
          token: data.authToken,
          provider: data.provider
        }
        localStorage.setItem('bijakImage', data.user.bijakImage);
        this.commonServ.post('api/auth/social', this.userData).subscribe((data:any) => {
          localStorage.setItem('token', JSON.stringify(data.token));
          localStorage.setItem('currentUser', jwt_decode(data.token).email);
          if(jwt_decode(data.token).is_admin) {
            this.toastr.success('Hello admin! Login to admin dashboard');
            this.logout();
          } else if(jwt_decode(data.token).is_seller) {
            this.toastr.success('login successfull');
            this.router.navigate(['/dashboard']);
          } else {
            this.toastr.success('Hello User! Login to User dashboard');
            this.logout();
          }
        },
        error => {
          this.toastr.error(error.error.message);
        });
      });
    }
  }

  checkSignin(status) {
    if(status == 'signin') {
      this.forgotPassword = false;
      this.signup = false;
      this.signin = true;
    } else if(status == 'signup') {
      this.forgotPassword = false;
      this.signup = true;
      this.signin = false;
    } else if(status == 'forgotPassword') {
      this.forgotPassword = true;
      this.signin = false;
      this.signup = false;
    }
  }

}
