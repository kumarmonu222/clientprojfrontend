import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllBijakComponent } from './all-bijak.component';

describe('AllBijakComponent', () => {
  let component: AllBijakComponent;
  let fixture: ComponentFixture<AllBijakComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllBijakComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllBijakComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
