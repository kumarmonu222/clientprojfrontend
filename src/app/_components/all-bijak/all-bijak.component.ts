import { Component, OnInit } from '@angular/core';
import { CommonServicesService } from 'src/app/_services/commonservice.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import * as jwt_decode from 'jwt-decode';
import { NgxSpinnerService } from "ngx-spinner";
import { DomSanitizer } from "@angular/platform-browser"; 

@Component({
  selector: 'app-all-bijak',
  templateUrl: './all-bijak.component.html',
  styleUrls: ['./all-bijak.component.scss']
})
export class AllBijakComponent implements OnInit {
  products: any;
  shopId: any;
  bijaks: any;
  singleBijak: any;
  selected: any;
  bijakImage: string;

  constructor(
    private commonServ: CommonServicesService,
    private formbuilder: FormBuilder, 
    public toastr: ToastrService,
    private spinner: NgxSpinnerService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.bijakImage = localStorage.getItem('bijakImage');
    this.commonServ.getSingle('api/shop/detail', jwt_decode(localStorage.getItem('token')).id).subscribe((data:any) => {
      this.shopId = data.shop._id;
      this.commonServ.getAll('api/bijak/specific-shop-bijaks/' + this.shopId).subscribe((data: any) =>{
        this.bijaks = data.bijak;
        this.commonServ.getSingle('api/bijak/', this.bijaks[0]._id).subscribe((data: any) => {
          this.singleBijak = data.bijak;
          this.selected = data.bijak._id;
        });
        this.spinner.hide();
      }, (error:any) => {
        if(error.error.message === "No Entries found") {
          this.spinner.hide();
        }
      });
    });
  }

  getBijak(bijakId) {
    this.commonServ.getSingle('api/bijak/', bijakId).subscribe((data: any) => {
      this.singleBijak = data.bijak;
      this.selected = data.bijak._id;
    });
  }

  // photoURL() {
  //   return this.sanitizer.bypassSecurityTrustUrl('/assets/images/jagdamba.jpg');
  // }

  isActive(item) {
    return this.selected === item;
  };
  
  deleteBijak(id) {
    this.commonServ.delete('api/bijak/delete', id).subscribe(
      (data:any) => {
        this.toastr.success(data.success);
        this.ngOnInit();
      },
      error => {
        this.toastr.error(error.error.message);
      }
    );
  }
}